﻿using System;

namespace BECore.Domain
{
    public interface IEntity
    {
        Guid Id { get; set; }
        DateTime Created { get; set; }
        Guid CreatorId { get; set; }
        DateTime Updated { get; set; }
        Guid UpdatorId { get; set; }
    }
}
