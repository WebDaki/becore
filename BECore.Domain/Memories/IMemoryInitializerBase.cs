﻿using System;

namespace BECore.Domain.Memories
{
    public interface IMemoryInitializerBase<T> : IDisposable where T : IAggregateRoot
    {
        void Initialize();
    }
}
