﻿using System;
using System.Collections.Generic;

namespace BECore.Domain.Memories
{
    public interface IMemoryRepositoryBase<T> where T : IAggregateRoot
    {
        List<T> FindAll();
        List<T> Find(List<Guid> ids);
        T Find(Guid id);
        void Remove(T item);
        void Remove(Guid id);
    }
}
