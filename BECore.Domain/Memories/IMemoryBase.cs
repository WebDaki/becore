﻿namespace BECore.Domain.Memories
{
    public interface IMemoryBase<T> : IMemoryInitializerBase<T>, IMemoryRepositoryBase<T>
        where T : IAggregateRoot
    { }
}
