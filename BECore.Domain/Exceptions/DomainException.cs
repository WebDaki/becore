﻿using System;

namespace BECore.Domain.Exceptions
{
    public class DomainException : Exception
    {
        public DomainException(
            string message = null,
            Exception exception = null
        )
            : base(message, exception)
        { }
    }
}
