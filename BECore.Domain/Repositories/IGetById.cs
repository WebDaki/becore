﻿using System;

namespace BECore.Domain.Repositories
{
    public interface IGetById<T> where T : IAggregateRoot
    {
        T ById(Guid id);
    }
}
