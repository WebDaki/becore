﻿using System.Collections.Generic;

namespace BECore.Domain.Repositories
{
    public interface IGetAll<T> where T : IAggregateRoot
    {
        IList<T> All();
    }
}
