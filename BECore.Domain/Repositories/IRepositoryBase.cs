﻿using System;

namespace BECore.Domain.Repositories
{
    public interface IRepositoryBase<T> : IGetById<T>, IGetAll<T> where T : IAggregateRoot
    {
        void Add(T aggregate);
        void Modify(T aggregate);
        void Delete(Guid id);
    }
}
