﻿using System;

namespace BECore.Domain
{
    public abstract class Event : IEvent
    {
        public Guid UserId { get; set; }

        public Event(Guid userId)
        {
            UserId = userId;
        }
    }
}
