﻿using System.Collections.Generic;

namespace BECore.Domain
{
    public interface IAggregateRoot : IEntity
    {
        int Version { get; set; }

        void AddEvent(IEvent eventToThrow);
        IEnumerable<IEvent> GetEvents();
    }
}
