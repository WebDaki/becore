﻿using System;

namespace BECore.Domain
{
    public abstract class Entity : IEntity
    {
        public Guid Id { get; set; }
        public DateTime Created { get; set; }
        public Guid CreatorId { get; set; }
        public DateTime Updated { get; set; }
        public Guid UpdatorId { get; set; }
    }
}
