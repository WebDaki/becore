﻿using System;

namespace BECore.Domain
{
    public interface IEvent
    {
        Guid UserId { get; set; }
    }
}
