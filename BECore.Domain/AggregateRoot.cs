﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace BECore.Domain
{
    public class AggregateRoot : Entity, IAggregateRoot
    {
        public int Version { get; set; }

        private ICollection<IEvent> _events = new Collection<IEvent>();
        public IEnumerable<IEvent> GetEvents()
        {
            var returningEvents = _events.ToList();
            _events = new Collection<IEvent>();
            return returningEvents;
        }

        public void AddEvent(IEvent eventToThrow)
        {
            _events.Add(eventToThrow);
        }
    }
}
