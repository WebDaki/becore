﻿namespace BECore.Domain.Events
{
    public interface IEventRunner
    {
        void Initialize();
    }
}
