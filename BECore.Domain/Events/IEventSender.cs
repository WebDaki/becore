﻿namespace BECore.Domain.Events
{
    public interface IEventSender
    {
        void Send(IEvent _event);
    }
}
