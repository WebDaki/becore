﻿using BECore.Application.Context;
using BECore.Infrastructure.Automaticals;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace BECore.Infrastructure
{
    public static class CoreConfigureServices
    {
        private static IServiceProvider _services = null;
        private static IEnumerable<Type> _appClasses = null;

        /// <summary>
        /// You must pass 'System.Reflection.Assembly.GetExecutingAssembly().GetReferencedAssemblies()' as 'assemblies' parameter
        /// </summary>
        /// <param name="services">IServiceCollection where prepare the injectable clases.</param>
        /// <param name="assemblies">List of assembly names where search the IInjectable clases.</param>
        public static void AutoConfigureServices(
            this IServiceCollection services,
            IEnumerable<AssemblyName> assemblyNames
        ) {
            services.AddHttpContextAccessor();
            PrepareAllClases(assemblyNames);
            InjectDepencences(services);
            InitializeAutomatics();
            SetApplicationContext(services);
        }

        private static void PrepareAllClases(IEnumerable<AssemblyName> assemblyNames)
        {
            var allTypes = assemblyNames.SelectMany(assembly => Assembly.Load(assembly.Name).GetTypes());

            _appClasses = allTypes.Where(t => !t.IsInterface && !t.IsAbstract);
        }

        private static void InjectDepencences(IServiceCollection services)
        {
            DependenceInjector.AutoInject(services, _appClasses);

            _services = services.BuildServiceProvider();
        }

        private static void InitializeAutomatics()
        {
            Initilizator.Run(_services, _appClasses);
        }

        private static void SetApplicationContext(IServiceCollection services)
        {
            ApplicationContext.Provider = services.BuildServiceProvider();
        }

        public static void Add(IServiceCollection services, Type interfaceToInject, Type classToInject)
        {
            services.TryAddScoped(interfaceToInject, classToInject);
        }

        public static void Override<I>(IServiceCollection services, I implementation)
        {
            try
            {
                var serviceDescriptor = services.FirstOrDefault(sd => sd.ServiceType == typeof(I));
                if (serviceDescriptor != null)
                    services.Remove(serviceDescriptor);
            }
            catch (Exception e) { }

            services.TryAddScoped(typeof(I), isp => implementation);
        }

        public static IT Resolve<IT>(ServiceCollection _serviceCollection = null)
        {
            var sp = (_serviceCollection == null ? _services : _serviceCollection.BuildServiceProvider());
            try
            {
                Type type = typeof(IT);

                if (type.IsClass)
                    type = DependenceInjector.GetExclusiveInterfaces(_appClasses, type).First();

                return (IT)sp.GetService(type);
            }
            catch (Exception e)
            {
                throw new Exception($"Core.Resolve<{ typeof(IT).Name }>(): Service '{ typeof(IT).Name }' Is not registered.", e);
            }
        }
    }
}
