﻿using BECore.Application.Automaticals;
using BECore.Application.Base;
using Microsoft.Extensions.Configuration;
using System;

namespace BECore.Infrastructure.Base
{
    public class NetCoreConfig : IConfig, IInjectable
    {
        private readonly IConfiguration _config;

        public NetCoreConfig(IConfiguration config)
        {
            _config = config;
        }

        public string Get(string key, string defaultValue = null)
        {
            string value = null;

            try { value = _config[key]; } catch (Exception e) { }

            return value ?? defaultValue ?? key;
        }
    }
}
