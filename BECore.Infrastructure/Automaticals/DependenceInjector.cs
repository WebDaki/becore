﻿using BECore.Application.Automaticals;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BECore.Infrastructure.Automaticals
{
    public class DependenceInjector
    {
        public static void AutoInject(IServiceCollection services, IEnumerable<Type> appClasses)
        {
            foreach (var type in appClasses)
                if (typeof(IInjectable).IsAssignableFrom(type))
                    InjectDependency(appClasses, services, type);
        }

        private static void InjectDependency(IEnumerable<Type> appClasses, IServiceCollection services, Type classToInject)
        {
            foreach (var interfaceToInject in GetExclusiveInterfaces(appClasses, classToInject))
                Add(services, interfaceToInject, classToInject);

            Add(services, classToInject, classToInject);
        }

        public static IEnumerable<Type> GetExclusiveInterfaces(IEnumerable<Type> appClasses, Type classToInject)
        {
            try
            {
                var interfaces = classToInject.GetInterfaces();
                return interfaces.Where(i => !appClasses.Any(c => c != classToInject && i.IsAssignableFrom(c)));
            }
            catch (Exception e)
            {
                throw new Exception($"DI.GetExclusiveInterfaces(): 'InjectDependencies(IServiceCollection services)' has not being invoqued.", e);
            }
        }

        public static void Add(IServiceCollection services, Type interfaceToInject, Type classToInject)
        {
            services.TryAddScoped(interfaceToInject, classToInject);
        }
    }
}
