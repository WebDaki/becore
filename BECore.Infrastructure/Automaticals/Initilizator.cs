﻿using BECore.Application.Automaticals;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BECore.Infrastructure.Automaticals
{
    public class Initilizator
    {
        public static void Run(IServiceProvider services, IEnumerable<Type> appClasses)
        {
            List<Task> tasks = new List<Task>();

            foreach (var initializableType in appClasses.Where(c => typeof(IInitializable).IsAssignableFrom(c)))
            {
                if (typeof(IProgramable).IsAssignableFrom(initializableType))
                    Programe(tasks, services, initializableType);

                else
                    tasks.Add(Task.Run(() => Initialize(services, initializableType)));
            }

            Task.WaitAll(tasks.ToArray());
        }

        private static void Programe(List<Task> tasks, IServiceProvider services, Type programableType)
        {
            try
            {
                var minutes = GetMinutesToReload(services, programableType);

                if (minutes > 0)
                    Task.Run(() => RunMethodRepeatedly(services, minutes, programableType));
            }
            catch (Exception e) { }
        }

        private static void RunMethodRepeatedly(IServiceProvider services, int minutes, Type programableType)
        {
            while (true)
            {
                try
                {
                    var service = (IProgramable)services.GetService(programableType);

                    service.Initialize();

                    if (service is IDisposable disposable)
                        disposable.Dispose();
                }
                catch (Exception e) { }

                Thread.Sleep(new TimeSpan(0, minutes, 0));
            }
        }

        private static int GetMinutesToReload(IServiceProvider services, Type programableType)
        {
            var minutes = -1;

            try
            {
                var service = (IProgramable)services.GetService(programableType);

                minutes = service.GetMinutesToReload();

                if (service is IDisposable disposable)
                    disposable.Dispose();
            }
            catch (Exception e) { }

            return minutes;
        }

        private static void Initialize(IServiceProvider services, Type initializableType)
        {
            try
            {
                var service = (IInitializable)services.GetService(initializableType);

                service.Initialize();

                if (service is IDisposable disposable)
                    disposable.Dispose();
            }
            catch (Exception e) { }
        }
    }
}
