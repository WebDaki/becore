﻿using BECore.Application.Automaticals;
using Microsoft.Extensions.Caching.Memory;
using System;

namespace BECore.Infrastructure.Memories
{
    public class Cache : ICache, IInjectable
    {
        private static MemoryCache _cache;
        private static object lockItem = new object();

        public Cache()
        {
            if (_cache == null)
                lock (lockItem)
                    if (_cache == null)
                        _cache = new MemoryCache(new MemoryCacheOptions());
        }

        public T Get<T>(string key)
        {
            return _cache.Get<T>(key);
        }

        public void Set<T>(string key, T item, TimeSpan time)
        {
            _cache.Set(key, item, time);
        }

        public void Remove(string key)
        {
            _cache.Remove(key);
        }
    }
}
