﻿using BECore.Application.Automaticals;
using BECore.Application.Base;
using BECore.Domain;
using BECore.Domain.Repositories;

namespace BECore.Infrastructure.Memories
{
    public abstract class RechargeableMemoryBase<T> : MemoryBase<T> , IProgramable where T : IAggregateRoot
    {
        private const int DEFAULT_RECHARGING_MINUTES = 60;
        private const string RECHARGING_MINUTES_CONFIG_KEY = "{0}RechargeableMemoryMinutes";

        private int _rechargingMinutes;

        public RechargeableMemoryBase(IConfig config, ICache cache, IGetById<T> byId, IGetAll<T> all)
            : base(config, cache, byId, all)
        {
            var value = config.Get(string.Format(RECHARGING_MINUTES_CONFIG_KEY, typeof(T).Name));

            if (int.TryParse(value, out int rechargingMinutes))
                _rechargingMinutes = rechargingMinutes;

            else
                _rechargingMinutes = DEFAULT_RECHARGING_MINUTES;
        }

        public int GetMinutesToReload()
        {
            return _rechargingMinutes;
        }
    }
}
