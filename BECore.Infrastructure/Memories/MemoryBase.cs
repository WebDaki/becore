﻿using BECore.Application.Base;
using BECore.Application.Disposable;
using BECore.Domain;
using BECore.Domain.Memories;
using BECore.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BECore.Infrastructure.Memories
{
    public abstract class MemoryBase<T> : AutoDisposable, IMemoryBase<T> where T : IAggregateRoot
    {
        private enum KeysListAction { Add = 1, Remove = 2 }

        private readonly ICache _cache;
        private readonly IGetById<T> _byId;
        private readonly IGetAll<T> _all;

        private int MinutesInMemory = 60;
        private object lockListModifications = new object();

        public MemoryBase(IConfig config, ICache cache, IGetById<T> byId, IGetAll<T> all)
        {
            _cache = cache;
            _byId = byId;
            _all = all;

            MinutesInMemory = int.Parse(config.Get($"{ typeof(T).Name }sMemory:MinutesInMemory", MinutesInMemory.ToString()));
        }

        public void Initialize()
        {
            var actualItems = _all.All().ToDictionary(item => item.Id, item => item);

            foreach (var obsoleteId in MemorizedIds().Where(id => !actualItems.ContainsKey(id)).ToList())
                Remove(obsoleteId);

            foreach (var item in actualItems)
                Set(item.Value);
        }

        public T Find(Guid id)
        {
            T item = _cache.Get<T>(GetCacheItemKey(id));

            if (item == null)
                item = Set(_byId.ById(id));

            return item;
        }

        public void Remove(T item)
        {
            Remove(item.Id);
        }

        public void Remove(Guid id)
        {
            _cache.Remove(GetCacheItemKey(id));

            UpdateKeysList(KeysListAction.Remove, id);
        }

        public List<T> FindAll()
        {
            return Find(MemorizedIds());
        }

        private List<Guid> MemorizedIds()
        {
            return _cache.Get<List<Guid>>(GetCacheListKey()) ?? new List<Guid>();
        }

        public List<T> Find(List<Guid> ids)
        {
            List<T> items = new List<T>();

            foreach (var id in ids)
            {
                var item = Find(id);
                if (item != null)
                    items.Add(item);
            }

            return items;
        }

        private T Set(T item)
        {
            if (item != null)
            {
                _cache.Set(
                    GetCacheItemKey(item.Id),
                    item,
                    GetCacheTime()
                );

                UpdateKeysList(KeysListAction.Add, item.Id);
            }

            return item;
        }

        private void UpdateKeysList(KeysListAction action, Guid id)
        {
            lock (lockListModifications)
            {
                var list = MemorizedIds();

                switch (action)
                {
                    case (KeysListAction.Add):
                        if (!list.Contains(id))
                            list.Add(id);
                        break;

                    case (KeysListAction.Remove):
                        if (list.Contains(id))
                            list.Remove(id);
                        break;

                    default:
                        break;
                }

                _cache.Set<List<Guid>>(
                    GetCacheListKey(),
                    list,
                    GetCacheTime()
                );
            }
        }

        private TimeSpan GetCacheTime()
        {
            return TimeSpan.FromSeconds(MinutesInMemory * 60);
        }

        public string GetCacheItemKey(Guid id)
        {
            return GetCacheKey(id.ToString());
        }

        public string GetCacheListKey()
        {
            return GetCacheKey("ALL");
        }

        public string GetCacheKey(string name)
        {
            return $"{ typeof(T).Name }_{ name }";
        }
    }
}
