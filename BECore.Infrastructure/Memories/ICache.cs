﻿using System;

namespace BECore.Infrastructure.Memories
{
    public interface ICache
    {
        void Set<T>(string key, T item, TimeSpan time);
        T Get<T>(string key);
        void Remove(string key);
    }
}
