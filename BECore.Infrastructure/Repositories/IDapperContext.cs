﻿using System;
using System.Collections.Generic;

namespace BECore.Infrastructure.Repositories
{
    public interface IDapperContext : IDisposable
    {
        List<T> Ask<T>(string query);
        List<object> Ask(Type type, string query);
        void Run(string command, object item = default);
    }
}