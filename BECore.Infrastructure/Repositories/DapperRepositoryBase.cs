﻿using BECore.Application.Base;
using BECore.Domain;
using BECore.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;

namespace BECore.Infrastructure.Repositories
{
    public abstract class DapperRepositoryBase<T> : DapperContext, IRepositoryBase<T> where T : IAggregateRoot
    {
        private const string
            SELECT_ALL = "SELECT * FROM {0}_{0}",
            SELECT_ALL_WHERE = "SELECT * FROM {0}_{1} WHERE {2}",
            SELECT_BY_ID = "SELECT * FROM {0}_{0} WHERE Id = '{1}'",
            SELECT_EMPTY = "SELECT * FROM {0}_{0} WHERE 1 = 0",
            SELECT_CHILDS = "SELECT * FROM {0}_{1} WHERE {2} IN (@ids){3}",
            DELETE_BY_ID = "DELETE FROM {0}_{0} WHERE Id = '{1}'",
            INSERT = "INSERT INTO {0}_{1} ({2}) VALUES ({3})";

        private readonly string aggregateName = typeof(T).Name;
        public DapperRepositoryBase(IConfig config, IDbConnection dbConnection, string DBConnectionStringConfigKey = null)
            : base(config, dbConnection, DBConnectionStringConfigKey)
        {

        }

        public void Add(T aggregate)
        {
            Insert(aggregate);
        }

        public void Modify(T aggregate)
        {
            Delete(aggregate.Id);
            Add(aggregate);
        }

        public void Delete(Guid id)
        {
            Run(string.Format(DELETE_BY_ID, aggregateName, id));
        }

        public IList<T> All()
        {
            return _SetChilds(Get<T>(string.Format(SELECT_ALL, aggregateName)));
        }

        public T ById(Guid id)
        {
            return _SetChilds(Get<T>(string.Format(SELECT_BY_ID, aggregateName, id))).FirstOrDefault();
        }

        public List<R> GetByCondition<R>(string WHERE) where R : IEntity
        {
            return Get<R>(string.Format(SELECT_ALL_WHERE, aggregateName, typeof(R).Name, WHERE));
        }

        private List<T> _SetChilds(List<T> items)
        {
            if (items == null || !items.Any())
                return new List<T>();

            return SetChilds(items);
        }

        public abstract List<T> SetChilds(List<T> items);

        public static void SetPrivateProperty<TEntity>(TEntity parent, string attributeName, object value)
            where TEntity : IEntity
        {
            var prop = parent.GetType().GetField(attributeName, BindingFlags.NonPublic | BindingFlags.Instance);
            if (prop == null)
                prop = parent.GetType().BaseType.GetField(attributeName, BindingFlags.NonPublic | BindingFlags.Instance);

            prop.SetValue(parent, value);
        }

        private Guid GetGuidField(object item, string fieldName = "Id")
        {
            return (Guid)item.GetType().GetProperty(fieldName).GetValue(item);
        }

        public Dictionary<Guid, List<ChildClass>> GetChildsAsDictionary<ChildClass, ParentClass>(List<Guid> ids, string extraConditions = null, string childClassNameInDDBB = null)
        {
            return GroupByParentId(
                Get<ChildClass>(
                    string.Format(
                        SELECT_CHILDS,
                        aggregateName,
                        string.IsNullOrEmpty(childClassNameInDDBB) ? typeof(ChildClass).Name : childClassNameInDDBB,
                        $"{ typeof(ParentClass).Name }Id",
                        string.IsNullOrEmpty(extraConditions) ? string.Empty : $" AND { extraConditions }"
                    ),
                    ids
                ),
                $"{ typeof(ParentClass).Name }Id"
            );
        }

        public Dictionary<Guid, List<R>> GroupByParentId<R>(IEnumerable<R> items, string parentIdFieldName)
        {
            var propSteps = parentIdFieldName.Split('.');

            var propNameForKeyValue = propSteps.Length > 1 ? propSteps[propSteps.Length - 1] : parentIdFieldName;

            return items.GroupBy(i => GetGuidField(i, propNameForKeyValue)).ToDictionary(g => g.Key, g => g.ToList());
        }

        public List<R> Get<R, K>(string query, List<K> keys = null, string key = "@ids")
        {
            if (keys == null)
                return Ask<R>(query);

            var returnList = new List<R>();

            for (int i = 0; i <= keys.Count; i += 1000)
            {
                var listKeys = keys.GetRange(i, Math.Min(1000, keys.Count - i));

                if (listKeys.Any())
                {
                    var stringKeys = CreateInQueryFromList(listKeys);

                    var splitedQuery = query.Replace(key, stringKeys);

                    var c = Ask<R>(splitedQuery);

                    returnList.AddRange(c);
                }
            }

            return returnList;
        }

        public List<R> Get<R>(string query, List<Guid> ids = null, string key = "@ids")
        {
            return Get<R, Guid>(query, ids, key);
        }

        public int Count<K>(string query, List<K> keys = null, string key = "@ids")
        {
            if (keys != null)
                query = query.Replace(key, CreateInQueryFromList(keys, false));

            return Count(query);
        }

        public int Count(string query)
        {
            return Get<int>(query).First();
        }

        public string CreateInQueryFromList<K>(List<K> keys, bool checkMaxKeysAmount = true)
        {
            string stringIds;

            if (checkMaxKeysAmount && keys.Count > 1000)//lo dejo asi vemos si se usa desde un punto incorrecto
                throw new Exception("In incorrecto 1000 elementos máximo ");

            else if (keys.Count == 0)
                stringIds = string.Format(SELECT_EMPTY, aggregateName); // No devuelve ningun elemento. Equivalente a dejarlo vacío, pero sin problemas al lanzarlo.

            else
                stringIds = $"'{ string.Join("','", keys.Select(id => id.ToString().Replace("'", "''"))) }'";

            return stringIds;
        }


        // INSERT Functionality

        private void Insert(object item)
        {
            Type itemType = item.GetType();
            List<object> childs = new List<object>();
            List<string> fields = new List<string>();

            foreach (var property in itemType.GetProperties())

                if (typeof(IEnumerable<IEntity>).IsAssignableFrom(property.PropertyType) && property.PropertyType.IsPublic && property.GetValue(item) is List<object> list)
                    childs.AddRange(list);

                else if (typeof(IEntity).IsAssignableFrom(property.PropertyType))
                    childs.Add(property.GetValue(item));

                else if (!typeof(IEnumerable<>).IsAssignableFrom(property.PropertyType))
                    fields.Add(property.Name);

            Run(string.Format(INSERT, aggregateName, itemType.Name, string.Join(", ", fields), string.Join(", ", fields.Select(f => $"@{ f }"))), item);

            foreach (var child in childs)
                Insert(child);
        }
    }
}
