﻿using BECore.Application.Automaticals;
using Dapper;
using System;
using System.Data;

namespace BECore.Infrastructure.Repositories
{
    public class MySqlGuidTypeForDapperHandler : SqlMapper.TypeHandler<Guid>, IInitializable
    {
        public void Initialize()
        {
            SqlMapper.AddTypeHandler(new MySqlGuidTypeForDapperHandler());
            SqlMapper.RemoveTypeMap(typeof(Guid));
            SqlMapper.RemoveTypeMap(typeof(Guid?));
        }

        public override void SetValue(IDbDataParameter parameter, Guid guid)
        {
            parameter.Value = guid.ToString();
        }

        public override Guid Parse(object value)
        {
            return new Guid((string)value);
        }
    }
}
