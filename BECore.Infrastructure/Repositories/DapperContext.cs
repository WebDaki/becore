﻿using BECore.Application.Base;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace BECore.Infrastructure.Repositories
{
    public class DapperContext : IDapperContext
    {
        private IDbConnection _dbConnection;
        private string DBConnectionString = "DDBB";

        public DapperContext(IConfig config, IDbConnection dbConnection, string DBConnectionStringConfigKey = null)
        {
            _dbConnection = dbConnection;

            DBConnectionString = config.Get(DBConnectionStringConfigKey, config.Get(DBConnectionString));

            if (string.IsNullOrEmpty(DBConnectionString))
                throw new Exception("Database not provided in config.");
        }

        public List<T> Ask<T>(string query)
        {
            OpenDatabaseConnection();

            var queryResult = _dbConnection.Query<T>(query);

            CloseDatabaseConnection();

            return queryResult.ToList();
        }

        public List<object> Ask(Type type, string query)
        {
            OpenDatabaseConnection();

            var queryResult = _dbConnection.Query(type, query);

            CloseDatabaseConnection();

            return queryResult.ToList();
        }

        public void Run(string command, object item = null)
        {
            OpenDatabaseConnection();

            _dbConnection.Execute(command, item);

            CloseDatabaseConnection();
        }

        private void OpenDatabaseConnection()
        {
            if (_dbConnection.State == ConnectionState.Closed)
            {
                _dbConnection.ConnectionString = DBConnectionString;
                _dbConnection.Open();
            }
        }

        private void CloseDatabaseConnection()
        {
            _dbConnection.Close();
        }

        public void Dispose()
        {
            if (_dbConnection != null)
                _dbConnection.Dispose();
        }
    }
}
