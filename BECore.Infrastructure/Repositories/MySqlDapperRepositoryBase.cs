﻿using BECore.Application.Base;
using BECore.Domain;
using MySql.Data.MySqlClient;

namespace BECore.Infrastructure.Repositories
{
    public abstract class MySqlDapperRepositoryBase<T> : DapperRepositoryBase<T> where T : IAggregateRoot
    {
        public MySqlDapperRepositoryBase(IConfig config, string DBConnectionStringConfigKey = null)
            : base(config, new MySqlConnection(), DBConnectionStringConfigKey) { }
    }
}
