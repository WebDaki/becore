﻿namespace BECore.Infrastructure.Events
{
    internal class RabbitConnectionConfiguration
    {
        public string Host { get; set; }
        public string User { get; set; }
        public string Pass { get; set; }
        public string VHost { get; set; }
        public string Uri { get; set; }
    }
}
