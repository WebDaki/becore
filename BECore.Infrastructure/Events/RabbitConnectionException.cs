﻿using BECore.Domain.Exceptions;

namespace BECore.Infrastructure.Events
{
    public class RabbitConnectionException : DomainException
    {
        public RabbitConnectionException() : base("Impossible to connect to RabbitMQ") { }
    }
}
