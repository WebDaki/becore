﻿namespace BECore.Infrastructure.Events
{
    public interface IEventTester
    {
        void TestConnection();
    }
}