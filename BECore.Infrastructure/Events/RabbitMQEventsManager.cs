﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using BECore.Application.Automaticals;
using BECore.Application.Base;
using BECore.Domain;
using BECore.Domain.Events;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace BECore.Infrastructure.Events
{
    public class RabbitMQEventsManager : IEventRunner, IEventSender, IEventTester, IInitializable
    {
        private static RabbitConnectionConfiguration _connectionConfiguration;
        private static object _connectionConfigurationLock = new object();
        private static ConnectionFactory _factory;
        private static object _factoryLock = new object();

        private const string
            BUS_CONFIG_KEY_APP = "EventsBus:App",
            BUS_CONFIG_KEY_RETRY_TIMES = "EventsBus:EventHandlingRetryTimes",
            BUS_CONFIG_KEY_SENDER = "EventsBus:Sender",
            BUS_CONFIG_KEY_CONFIGURATION = "EventsBus:EventsConfig",
            BUS_CONFIG_KEY_RABBITMQ_HOST = "EventsBus:RabbitMQ:HostName",
            BUS_CONFIG_KEY_RABBITMQ_USER = "EventsBus:RabbitMQ:UserName",
            BUS_CONFIG_KEY_RABBITMQ_PASS = "EventsBus:RabbitMQ:Password",
            BUS_CONFIG_KEY_RABBITMQ_VHOST = "EventsBus:RabbitMQ:VirtualHost",
            BUS_CONFIG_KEY_RABBITMQ_URL = "EventsBus:RabbitMQ:Uri";

        private const int
            RETRY_TIMES = 5;

        private static string _app;
        private static string _boundedContent;
        private static int _retryTimes;

        private readonly IServiceProvider _provider;
        private Dictionary<string, Dictionary<Type, List<Type>>>
            _handlersForEvents = new Dictionary<string, Dictionary<Type, List<Type>>>();

        public RabbitMQEventsManager(IServiceProvider provider, IConfig config)
        {
            _provider = provider;

            if (_connectionConfiguration == null)
                lock (_connectionConfigurationLock)
                    if (_connectionConfiguration == null)
                        PrepareManager(config);
        }

        private void PrepareManager(IConfig config)
        {
            _app = config.Get(BUS_CONFIG_KEY_APP);

            _boundedContent = config.Get(BUS_CONFIG_KEY_SENDER);

            _connectionConfiguration = new RabbitConnectionConfiguration()
            {
                Host = config.Get(BUS_CONFIG_KEY_RABBITMQ_HOST),
                User = config.Get(BUS_CONFIG_KEY_RABBITMQ_USER),
                Pass = config.Get(BUS_CONFIG_KEY_RABBITMQ_PASS),
                VHost = config.Get(BUS_CONFIG_KEY_RABBITMQ_VHOST, "/"),
                Uri = config.Get(BUS_CONFIG_KEY_RABBITMQ_URL, string.Empty).Replace("amqp://", "amqps://")
            };

            ReadConfiguration(config);
        }

        private void ReadConfiguration(IConfig config)
        {
            var configuration = config.Get(BUS_CONFIG_KEY_CONFIGURATION);

            if (configuration == BUS_CONFIG_KEY_CONFIGURATION)
                return;

            LoadRetryTimesValue(config);

            configuration = configuration.Replace(" ", string.Empty);
            configuration = configuration.Replace("\n\r", string.Empty);

            foreach (var boundedContentConfigString in configuration.Split(';'))
                ParseBoundedContentEventsConfig(boundedContentConfigString);
        }

        private void LoadRetryTimesValue(IConfig config)
        {
            if (int.TryParse(config.Get(BUS_CONFIG_KEY_RETRY_TIMES), out int retries))
                _retryTimes = retries;

            else
                _retryTimes = RETRY_TIMES;
        }

        private void ParseBoundedContentEventsConfig(string boundedContentConfigString)
        {
            if (string.IsNullOrEmpty(boundedContentConfigString))
                return;

            var boundedContentConfig = boundedContentConfigString.Split(':');

            var boundedContent = boundedContentConfig[0];

            foreach (var eventName in boundedContentConfig[1].Split(','))
                ParseEventConfig(boundedContent, eventName);
        }

        private void ParseEventConfig(string boundedContent, string eventName)
        {
            Tuple<Type, List<Type>> tuple = GetEventHandlers(eventName);

            if (tuple == null || tuple.Item1 == null || tuple.Item2 == null || !tuple.Item2.Any())
                return;

            Type eventType = tuple.Item1;
            List<Type> handlers = tuple.Item2;

            if (!_handlersForEvents.ContainsKey(boundedContent))
                _handlersForEvents.Add(boundedContent, new Dictionary<Type, List<Type>>());

            if (!_handlersForEvents[boundedContent].ContainsKey(eventType))
                _handlersForEvents[boundedContent].Add(eventType, new List<Type>());

            _handlersForEvents[boundedContent][eventType].AddRange(handlers);
        }

        public void Initialize()
        {
            if (!_handlersForEvents.Any())
                return;

            var connection = CreateConnection();

            foreach (var kvpBCHandlers in _handlersForEvents)
            {
                var boundedContent = kvpBCHandlers.Key;

                foreach (var kvpEHandlers in kvpBCHandlers.Value)
                {
                    var eventType = kvpEHandlers.Key;
                    var handlers = kvpEHandlers.Value;
                    LaunchListeners(connection, boundedContent, eventType, handlers);
                }
            }
        }

        private void LaunchListeners(IConnection connection, string boundedContent, Type eventType, List<Type> handlers)
        {
            var channel = connection.CreateModel();

            string
                exchangeName = GetExchangeName(boundedContent, eventType),
                queueName = _boundedContent;

            channel.ExchangeDeclare(exchange: exchangeName, type: ExchangeType.Fanout);

            channel.QueueDeclare(
                queue: queueName,
                durable: false,
                exclusive: false,
                autoDelete: false,
                arguments: null
            );

            channel.QueueBind(queue: queueName,
                              exchange: exchangeName,
                              routingKey: "");

            Console.WriteLine(" [*] Waiting for logs.");

            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += (model, ea) => RunHandlers(handlers, eventType, ea);
            channel.BasicConsume(queue: queueName,
                                 autoAck: true,
                                 consumer: consumer);
        }

        private void RunHandlers(List<Type> handlers, Type eventType, BasicDeliverEventArgs ea)
        {
            foreach (var handlerType in handlers)
                Task.Run(() => { ExecuteEventHandler(handlerType, eventType, ea); });
        }

        private void ExecuteEventHandler(Type handlerType, Type eventType, BasicDeliverEventArgs ea)
        {
            var handler = _provider.GetService(handlerType);

            if (handler == null)
                return;

            TryRunMethod(handlerType.GetMethod("Resolve"), handler, eventType, ea);

            if (handler is IDisposable disposable)
                disposable.Dispose();
        }

        private void TryRunMethod(MethodInfo method, object handler, Type eventType, BasicDeliverEventArgs ea)
        {
            int retry = 0;
            int randomMilliseconds = 0;

            Exception exception = null;
            while (retry < _retryTimes)
            {
                Task.Delay(randomMilliseconds).Wait();

                try
                {
                    object[] parameters = { ReadEvent(ea, eventType) };

                    method.Invoke(handler, parameters);

                    retry = _retryTimes + 1;

                    exception = null;
                }
                catch (Exception e)
                {
                    retry++;

                    randomMilliseconds = new Random().Next(500, 5000);

                    exception = e;
                }

            }

            if (exception != null) { /* TODO */ }
        }

        private IEvent ReadEvent(BasicDeliverEventArgs ea, Type eventType)
        {
            var body = ea.Body;

            var message = Encoding.UTF8.GetString(body.ToArray());

            return (IEvent)JsonConvert.DeserializeObject(message, eventType);
        }

        public void Send(IEvent _event)
        {
            var connection = CreateConnection();
            var channel = connection.CreateModel();

            string exchangeName = GetSendingExchangeName(_event);

            channel.ExchangeDeclare(
                exchange: exchangeName,
                type: ExchangeType.Fanout
            );

            string message = JsonConvert.SerializeObject(_event);

            var body = Encoding.UTF8.GetBytes(message);

            channel.BasicPublish(
                exchange: exchangeName,
                routingKey: "",
                basicProperties: null,
                body: body
            );

            channel.Close();
            connection.Close();
        }

        private IConnection CreateConnection()
        {
            if (_factory == null)
                lock (_factoryLock)
                    if (_factory == null)
                        CreateFactory();

            return _factory.CreateConnection();
        }

        private void CreateFactory()
        {
            TryFactoryByUri();
            TryFactoryByUserPass();

            if (_factory == null)
                throw new RabbitConnectionException();
        }

        private void TryFactoryByUri()
        {
            if (_factory == null
                && !string.IsNullOrEmpty(_connectionConfiguration.Uri)
            )
            {
                try
                {
                    _factory = new ConnectionFactory();
                    _factory.Uri = new Uri(_connectionConfiguration.Uri);
                    var connection = _factory.CreateConnection();
                    connection.Close();
                }
                catch (Exception e)
                {
                    _factory = null;
                }
            }
        }

        private void TryFactoryByUserPass()
        {
            if (_factory == null
                && !string.IsNullOrEmpty(_connectionConfiguration.Host)
                && !string.IsNullOrEmpty(_connectionConfiguration.User)
                && !string.IsNullOrEmpty(_connectionConfiguration.Pass)
                && !string.IsNullOrEmpty(_connectionConfiguration.VHost)
            )
            {
                try
                {
                    _factory = new ConnectionFactory()
                    {
                        HostName = _connectionConfiguration.Host,
                        UserName = _connectionConfiguration.User,
                        Password = _connectionConfiguration.Pass,
                        VirtualHost = _connectionConfiguration.VHost
                    };
                    var connection = _factory.CreateConnection();
                    connection.Close();
                }
                catch (Exception e)
                {
                    _factory = null;
                }
            }
        }

        private string GetSendingExchangeName(IEvent @event)
        {
            return GetExchangeName(_boundedContent, @event.GetType());
        }

        private string GetExchangeName(string boundedContent, Type eventType)
        {
            return $"{ _app }.{ boundedContent.ToString() }.{ eventType.Name }";
        }

        public Tuple<Type, List<Type>> GetEventHandlers(string eventName)
        {
            var types = AppDomain.CurrentDomain.GetAssemblies().SelectMany(a => a.GetTypes());

            var eventType = types.FirstOrDefault(t => t.Name == eventName && typeof(IEvent).IsAssignableFrom(t));

            var handlers = types.Where(t => typeof(IInjectable).IsAssignableFrom(t) && t.Name.Contains(GetEventHandlerName(eventName))).ToList();

            return new Tuple<Type, List<Type>>(eventType, handlers);
        }

        private string GetEventHandlerName(string eventTypeName)
        {
            return $"{ eventTypeName }EventHandler";
        }

        public void TestConnection()
        {
            CreateConnection();
        }
    }
}