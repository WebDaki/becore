﻿using BECore.Application.Automaticals;
using BECore.Application.Base;
using BECore.Application.Hashers;
using System;

namespace BECore.Infrastructure.Hashers
{
    public class PasswordHasher : IPasswordHasher, IInjectable
    {
        private const string SEED = "PasswordHasher.Seed";

        private readonly IConfig _config;
        private readonly IHasher _hasher;

        public PasswordHasher(IConfig config, IHasher hasher)
        {
            _config = config;
            _hasher = hasher;
        }

        public string HashPassord(string userName, string plainPass)
        {
            string
                seed = _config.Get(SEED, SEED),
                wordToHash = string.Empty;

            for (int i = 0; i < Math.Max(Math.Max(userName.Length, plainPass.Length), seed.Length); i++)
                wordToHash += CharAt(userName, i) + CharAt(plainPass, i) + CharAt(seed, i);

            return _hasher.Hash(wordToHash);
        }

        private string CharAt(string word, int i)
        {
            return word.Length > i ? word[i].ToString() : " ";
        }
    }
}
