﻿using BECore.Application.Automaticals;
using BECore.Application.Base;
using BECore.Application.Hashers;
using System;
using System.Security.Cryptography;
using System.Text;

namespace BECore.Infrastructure.Hashers
{
    public class Hasher : IHasher, IInjectable
    {
        private const string SEED = "Hasher.Seed";

        private readonly IConfig _config;

        public Hasher(IConfig config)
        {
            _config = config;
        }

        public string Hash(string word)
        {
            string
                seed = _config.Get(SEED, SEED),
                wordToHash = string.Empty;

            for (int i = 0; i < Math.Max(word.Length, seed.Length); i++)
                wordToHash += CharAt(word, i) + CharAt(seed, i);

            return Process(wordToHash);
        }

        private string CharAt(string word, int i)
        {
            return word.Length > i ? word[i].ToString() : " ";
        }

        private string Process(string rawData)
        {
            // Create a SHA256   
            using (SHA256 sha256Hash = SHA256.Create())
            {
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(rawData));

                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                    builder.Append(bytes[i].ToString("x2"));

                return builder.ToString();
            }
        }
    }
}
