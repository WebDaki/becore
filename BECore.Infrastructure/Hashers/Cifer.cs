﻿using BECore.Application.Hashers;
using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace BECore.Infrastructure.Hashers
{
    public class Cipher : ICifer
    {
        private enum ProcessMode { Encrypt = 1, Decrypt = 2 }

        // Set your salt here, change it to meet your flavor:
        // The salt bytes must be at least 8 bytes.
        private readonly static byte[] saltBytes = new byte[] { 243, 22, 45, 10, 143, 89, 201, 176 };

        /// <summary>
        /// Encrypt a string.
        /// </summary>
        /// <param name="plainText">String to be encrypted</param>
        /// <param name="password">Password</param>
        public string Encrypt(string plainText, string password = null)
        {
            return Process(plainText, password, ProcessMode.Encrypt);
        }

        /// <summary>
        /// Decrypt a string.
        /// </summary>
        /// <param name="encryptedText">String to be decrypted</param>
        /// <param name="password">Password used during encryption</param>
        /// <exception cref="FormatException"></exception>
        public string Decrypt(string encryptedText, string password = null)
        {
            return Process(encryptedText, password, ProcessMode.Decrypt);
        }

        private static string Process(string textToProcess, string password, ProcessMode processMode)
        {
            if (textToProcess == null)
                return null;

            if (password == null)
                password = String.Empty;

            var bytesToBeProcessed = processMode == ProcessMode.Decrypt
                ? Convert.FromBase64String(textToProcess)
                : Encoding.UTF8.GetBytes(textToProcess);

            var passwordBytes = Encoding.UTF8.GetBytes(password);

            passwordBytes = SHA256.Create().ComputeHash(passwordBytes);

            var processedBytes = Cipher.ProcessBytes(bytesToBeProcessed, passwordBytes, processMode);

            return processMode == ProcessMode.Decrypt
                ? Encoding.UTF8.GetString(processedBytes)
                : Convert.ToBase64String(processedBytes);
        }

        private static byte[] ProcessBytes(byte[] bytesToProcess, byte[] passwordBytes, ProcessMode processMode)
        {
            byte[] processedBytes = null;

            using (MemoryStream ms = new MemoryStream())
            {
                using (RijndaelManaged AES = new RijndaelManaged())
                {
                    var key = new Rfc2898DeriveBytes(passwordBytes, saltBytes, 1000);

                    AES.KeySize = 256;
                    AES.BlockSize = 128;
                    AES.Key = key.GetBytes(AES.KeySize / 8);
                    AES.IV = key.GetBytes(AES.BlockSize / 8);
                    AES.Mode = CipherMode.CBC;

                    var cryptoTransform = processMode == ProcessMode.Decrypt ? AES.CreateDecryptor() : AES.CreateEncryptor();

                    using (var cs = new CryptoStream(ms, cryptoTransform, CryptoStreamMode.Write))
                    {
                        cs.Write(bytesToProcess, 0, bytesToProcess.Length);
                        cs.Close();
                    }

                    processedBytes = ms.ToArray();
                }
            }

            return processedBytes;
        }
    }
}
