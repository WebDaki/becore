﻿using Newtonsoft.Json;
using System;
using System.Net.Http;

namespace BECore.Infrastructure.WebApis
{
    public class WebApiConnector : IWebApiConnector
    {
        private const double DEFAULT_TIMEOUT_MILLISECONDS = 1000;
        private double? _defaultTimeoutMilliSeconds;

        public WebApiConnector(double? defaultTimeoutMilliSeconds = null)
        {
            _defaultTimeoutMilliSeconds = defaultTimeoutMilliSeconds;
        }

        public Tout Get<Tout>(string url, double? timeOutMilliSeconds = null)
        {
            try
            {
                HttpClient client = GetClient(timeOutMilliSeconds);

                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, url);

                HttpResponseMessage response = client.SendAsync(request).Result;

                if (response.IsSuccessStatusCode)
                    return JsonConvert.DeserializeObject<Tout>(response.Content.ReadAsStringAsync().Result);

                else
                    throw new Exception($"WebApiConnector.Get: [ Http Error --> StatusCode: { (int)response.StatusCode } - { response.StatusCode } | Url: '{ url }' ]");
            }
            catch (Exception ex)
            {
                throw new Exception($"WebApiConnector.Get: [ Unhandled Error --> Url: '{ url }' ]", ex);
            }

        }

        public Tout Post<Tout>(string url, object body, double? timeOutMilliSeconds = null)
        {
            try
            {
                HttpClient client = GetClient(timeOutMilliSeconds);

                HttpResponseMessage response = client.PostAsJsonAsync(url, body).Result;

                if (response.IsSuccessStatusCode)
                    return JsonConvert.DeserializeObject<Tout>(response.Content.ReadAsStringAsync().Result);

                else
                    throw new Exception($"WebApiConnector.Post: [ Http Error --> StatusCode: { (int)response.StatusCode } - { response.StatusCode } | Url: '{ url }' | Body: '{ body.ToString() }' ]");
            }
            catch (Exception ex)
            {
                throw new Exception($"WebApiConnector.Post: [ Unhandled Error --> Url: '{ url }' | Body: '{ body.ToString() }' ]", ex);
            }
        }

        private HttpClient GetClient(double? timeOutMilliSeconds = null)
        {
            HttpClient client = new HttpClient();

            if (timeOutMilliSeconds.HasValue && timeOutMilliSeconds.Value > 0)
                client.Timeout = TimeSpan.FromMilliseconds(timeOutMilliSeconds.Value);

            else if (_defaultTimeoutMilliSeconds.HasValue && _defaultTimeoutMilliSeconds.Value > 0)
                client.Timeout = TimeSpan.FromMilliseconds(_defaultTimeoutMilliSeconds.Value);

            else
                client.Timeout = TimeSpan.FromMilliseconds(DEFAULT_TIMEOUT_MILLISECONDS);

            return client;
        }
    }
}
