﻿namespace BECore.Infrastructure.WebApis
{
    public interface IWebApiConnector
    {
        Tout Get<Tout>(string url, double? timeOutMilliSeconds = null);
        Tout Post<Tout>(string url, object body, double? timeOutMilliSeconds = null);
    }
}
