﻿using System;

namespace BECore.Infrastructure.WebApis.Dtos
{
    public class WebApiIdDto : WebApiDto
    {
        public Guid Id { get; set; }
    }
}
