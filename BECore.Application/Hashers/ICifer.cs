﻿namespace BECore.Application.Hashers
{
    public interface ICifer
    {
        /// <summary>
        /// Encrypt a string.
        /// </summary>
        /// <param name="plainText">String to be encrypted</param>
        /// <param name="password">Password</param>
        string Encrypt(string plainText, string password = null);

        /// <summary>
        /// Decrypt a string.
        /// </summary>
        /// <param name="encryptedText">String to be decrypted</param>
        /// <param name="password">Password used during encryption</param>
        /// <exception cref="FormatException"></exception>
        string Decrypt(string encryptedText, string password = null);
    }
}
