﻿namespace BECore.Application.Hashers
{
    public interface IPasswordHasher
    {
        string HashPassord(string userName, string plainPass);
    }
}
