﻿namespace BECore.Application.Hashers
{
    public interface IHasher
    {
        string Hash(string word);
    }
}
