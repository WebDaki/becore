﻿using System;

namespace BECore.Application.Context
{
    public static class ApplicationContext
    {
        private static IServiceProvider _provider;
        private static string _name;

        public static IServiceProvider Provider
        {
            get { return _provider; }
            set { if (_provider == null) _provider = value; }
        }

        public static string Name
        {
            get { return _name; }
            set { if (_name == null) _name = value; }
        }
    }
}
