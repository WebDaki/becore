﻿namespace BECore.Application.Automaticals
{
    public interface IProgramable : IInitializable
    {
        int GetMinutesToReload();
    }
}
