﻿namespace BECore.Application.Automaticals
{
    public interface IInitializable : IInjectable
    {
        void Initialize();
    }
}
