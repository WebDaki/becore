﻿using BECore.Application.Automaticals;
using BECore.Application.Handlers;
using BECore.Application.Message;
using System;

namespace BECore.Application.Dispatchers
{
    public class QueryDispatcher : IQueryDispatcher, IInjectable
    {
        private readonly IServiceProvider _provider;
        public QueryDispatcher(IServiceProvider provider)
        {
            _provider = provider;
        }

        public TResult Ask<TQuery, TResult>(TQuery query)
            where TQuery : IQuery
            where TResult : IQueryResult
        {
            IQueryHandler<TQuery, TResult> handler = null;
            TResult result = default(TResult);

            try
            {
                handler = GetHandler<TQuery, TResult>();
                result = handler.Retrieve(query);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (handler != null)
                    handler.Dispose();
            }

            return result;
        }

        private IQueryHandler<TQuery, TResult> GetHandler<TQuery, TResult>()
            where TQuery : IQuery
            where TResult : IQueryResult
        {
            try
            {
                return (IQueryHandler<TQuery, TResult>)_provider.GetService(typeof(IQueryHandler<TQuery, TResult>));
            }
            catch (Exception e)
            {
                throw new Exception($"QueryHandler not registered for '<{ typeof(TQuery).Name }, { typeof(TResult).Name }>' <Query, Result>.", e);
            }
        }
    }
}
