﻿using BECore.Application.Message;

namespace BECore.Application.Dispatchers
{
    public interface IQueryDispatcher
    {
        TResult Ask<TQuery, TResult>(TQuery query) where TQuery : IQuery where TResult : IQueryResult;
    }
}
