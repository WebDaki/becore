﻿using BECore.Application.Automaticals;
using BECore.Application.Handlers;
using BECore.Application.Message;
using System;

namespace BECore.Application.Dispatchers
{
    public class CommandDispatcher : ICommandDispatcher, IInjectable
    {
        private readonly IServiceProvider _provider;
        public CommandDispatcher(IServiceProvider provider)
        {
            _provider = provider;
        }

        public void Dispatch<TCommand>(TCommand command) where TCommand : ICommand
        {
            ICommandHandler<TCommand> handler = null;

            try
            {
                handler = GetHandler<TCommand>();
                handler.Execute(command);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (handler != null)
                    handler.Dispose();
            }
        }

        private ICommandHandler<TCommand> GetHandler<TCommand>() where TCommand : ICommand
        {
            try
            {
                return (ICommandHandler<TCommand>)_provider.GetService(typeof(ICommandHandler<TCommand>));
            }
            catch (Exception e)
            {
                throw new Exception($"CommandHandler not registered for '{ typeof(TCommand).Name }' Command.", e);
            }
        }
    }
}
