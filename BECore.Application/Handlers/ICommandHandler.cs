﻿using BECore.Application.Message;
using System;

namespace BECore.Application.Handlers
{
    public interface ICommandHandler<TCommand> : IDisposable where TCommand : ICommand
    {
        void Execute(TCommand command);
    }
}
