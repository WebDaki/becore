﻿using BECore.Application.Message;
using System;

namespace BECore.Application.Handlers
{
    public interface IQueryHandler<TQuery, TResult> : IDisposable
        where TQuery : IQuery
        where TResult : IQueryResult
    {
        TResult Retrieve(TQuery query);
    }
}
