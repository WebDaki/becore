﻿using BECore.Domain;
using System;

namespace BECore.Application.Handlers
{
    public interface IEventHandler<TEvent> : IDisposable
        where TEvent : IEvent
    {
        void Resolve(TEvent _event);
    }
}
