﻿namespace BECore.Application.Base
{
    public interface IConfig
    {
        // Searchs the value in config for key. If it doesn't exist, returns defaultValue. If defaulValue is null returns key.
        string Get(string key, string defaultValue = null);
    }
}
