﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BECore.Application.Message
{
    public class Grouped<TKey, TElement>
    {
        public TKey Key { get; set; }

        public List<TElement> Values { get; set; }

        public Grouped(IGrouping<TKey, TElement> grouping)
        {
            if (grouping == null)
                throw new ArgumentNullException("grouping");
            Key = grouping.Key;
            Values = grouping.ToList();
        }

        public Grouped(TKey key, IEnumerable<TElement> list)
        {
            Key = key;
            Values = list.ToList();
        }
    }
}
