﻿using System.Collections.Generic;

namespace BECore.Application.Message
{
    public class PagedResult<T> : QueryResult<IEnumerable<T>>
    {
        public int TotalItems { get; set; }
        public int TotalPages { get; set; }
    }
}
