﻿namespace BECore.Application.Message
{
    public interface ICommandType { }
    public interface ICreate : ICommandType { }
    public interface IModify : ICommandType { }
    public interface IDelete : ICommandType { }
}
