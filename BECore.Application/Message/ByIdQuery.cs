﻿using System;

namespace BECore.Application.Message
{
    public class ByIdQuery : IQuery
    {
        public Guid Id { get; set; }
    }
}
