﻿namespace BECore.Application.Message
{
    public class QueryResult<T> : IQueryResult
    {
        public T Result { get; set; }
    }
}
