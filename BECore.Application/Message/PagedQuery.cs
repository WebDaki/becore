﻿namespace BECore.Application.Message
{
    public class PagedQuery : IQuery
    {
        public int Page { get; set; }
        public int PageLength { get; set; }
        public string OrderByField { get; set; }
        public bool Descending { get; set; }
        public string SearchString { get; set; }
    }
}
